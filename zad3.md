---
author: Bernard Kordas
title: CRISPR-Cas9
subtitle: Inżynieria genetyczna dla każdego.
date: 11.2020
theme: Warsaw
output: beamer_presentation
header-includes:
---



## Slajd nr 1

**Metoda CRISPR/Cas** - metoda inżynierii genetycznej,pozwalająca na manipulacje genomem danego organizmu.  
Mechanizm immunologiczny bakterii, na podstawie którego powstała owa metoda.  
W przypadku metody **CRISPR** możliwe jest dokonywanie bardzo _precyzyjnej_ edycji genomu docelowej komórki. Jest to poza tym metoda stosunkowo tania, wydajna i prosta w wykonaniu.Jedyne, co jest wymagane przy wykonywaniu transformacji genetycznej, to odpowiednie gRNA oraz dostępność enzymu Cas (najczęściej Cas9) w komórce docelowej.

## Slajd nr 2
Oto pierwszy z wykorzystanych w tej prezentacji rysunków.  
Przybliża krok po kroku mechanizm i rozwiewa wszelkie wątpliwości.  
![](cas9.png){height=400 width=215}

## Slajd nr 3

CRISPR z języka angielskiego to: _clustered regularly interspaced short palindromic repeats_  
Naukowcy przez wiele lat starali się określić rolę loci CRISPR. Początkowo uważano, że mechanizm ten odpowiada za naprawę DNA oraz regulację ekspresji genów.  
Badania przeprowadzone w 2002 roku pozwoliły naukowcom zidentyfikować białka Cas _(ang. CRISPR associated proteins)_ związane z mechanizmem CRISPR, które wykazują aktywność helikaz oraz nukleaz.

## Slajd nr 4

![](crispr.jpg){height=450 width=250}

## Slajd nr 5

Metoda ta bywa także nazywana "nożyczkami molekularnymi" ze względu na sposób, w jaki działa - wycinając specyficznie fragmenty genomu komórki.  
W kolejnym slajdzie przyjrzymy się przykładowej tabelce o zerowej wartości merytorycznej.  
Stanowi ona jednak **dowód**, że potrafię użyć tabelki w prezentacji pisanej MD.

## Slajd nr 6

|    Tabela|Udaje    | Znaczenie  |
| -------------|:-------------:| -----:|
|    CAS-9|kaspaza-9    | RNA |
|    CRISPR|kompleks      | DNA |

## Slajd nr 7

Elementy wchodzące w skład struktury matrycy - locus CRISPR warunkujące właściwości immunogenne tego systemu to:
• sekwencje powtórzone (R, ang. direct repeats) - ciąg identycznych sekwencji, które są ściśle konserwowane w danym locus, posiadają wielkość od 26 do 72 pz;
• sekwencje rozdzielające (S, ang. spacers) - sekwencje o wielkości 21-72 pz, występujące pomiędzy sekwencjami powtórzonymi;
• sekwencja liderowa - niekodująca sekwencja, zawierająca promotor sprawiający, że transkrypcja regionu CRISPR staje się możliwa;
• geny cas - zespół genów kodujących białka, zawierające funkcjonalne domeny, które są typowe dla nukleaz, helikaz, polimeraz oraz innych białek wiążących kwasy nukleinowe

## Slajd nr 8

Poniżej możemy zobaczyć komputerową wizualizację molekuły CRISPR-Cas9.
![](molekula.jpg){height=250 width=375}  
Pomoże ona zrozumieć mechanizm działania kompleksu na poziomie molekularnym.

## Slajd nr 8
    
  
# Serdecznie dziękuję za uwagę!
  